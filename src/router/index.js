import Vue from "vue"
import VueRouter from "vue-router"
Vue.use(VueRouter)

const routes = [
    {
        path: "/",
        redirect: '/login'
    },
    {
        path: "/login",
        component: () => import("@/views/login.vue")
    },
    {
        path: "/register",
        component: () => import("@/views/register.vue")
    },
    {
        path: "/personal",
        component: () => import("@/views/personal.vue")
    },
    {
        path: "/edit_profile",
        component: () => import("@/views/edit_profile.vue")
    }
]

const router = new VueRouter({

    routes
})

export default router