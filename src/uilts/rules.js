const rules = {
    phoneReg: /^[1][3,4,5,7,8,9][0-9]{9}$/,
    passwordReg: /^(\w){3,16}$/
}

export default rules