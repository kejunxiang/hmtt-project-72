import Vue from 'vue'
import App from './App.vue'
// 引入全局样式
import "./style/reset.less"
Vue.config.productionTip = false

// 按需引入vant模块
import { Button, Toast, Cell, CellGroup, NavBar, Icon, Uploader, Field, Dialog, ActionSheet } from 'vant';
Vue.use(Button);
Vue.use(Toast);
Vue.use(Cell);
Vue.use(CellGroup);
Vue.use(NavBar);
Vue.use(Icon);
Vue.use(Uploader);
Vue.use(Field);
Vue.use(Dialog);
Vue.use(ActionSheet);
// 全局注册黑马输入框插件
import hmInput from './components/hmInput.vue'
Vue.component('hmInput', hmInput)

// 全局注册日期过滤器
import moment from 'moment';
Vue.filter('format', (date) => {
  return moment(date).format('YYYY-MM-D HH:mm:ss')
})

// 全局注册拼接路径过滤器,基础路径加上返回路径
import axios from './uilts/request'
Vue.filter('pathJoin', (url) => {
  if (!url) {
    return url
  }
  if (url.indexOf('http') == -1) {
    return axios.defaults.baseURL + url
  } else {
    return url
  }
})

// 导入配置好的路由规则注入实例
import router from "./router/index"
new Vue({
  router,
  render: h => h(App),
}).$mount('#app')