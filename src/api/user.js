import axios from '../uilts/request'
// 登录请求
export function login(data) {
    return axios({
        url: '/login',
        method: 'post',
        data
    })
}
// 注册请求
export function register(data) {
    return axios({
        url: '/register',
        method: 'post',
        data
    })
}

// 获取用户信息
export function getUresInfo(id) {
    return axios({
        url: '/user/' + id,
        method: 'get',
    })
}

// 文件上传
export function upload(data) {
    return axios({
        url: '/upload',
        method: 'POST',
        data
    })
}

// 编辑用户信息
export function editUresInfo(id, data) {
    return axios({
        url: '/user_update/' + id,
        method: 'POST',
        data
    })
}