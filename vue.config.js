module.exports = {
    devServer: { // 自定义服务配置
        open: true, // 自动打开浏览器
        port: 8080
    },
    lintOnSave: false//关闭lint检查
}